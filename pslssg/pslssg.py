import argparse
import os
import logging
import shutil
import xml.dom.minidom as minidom
import xml
import sys
import http.server
import socketserver
import sqlite3
#also: pdb, unittest, pydoc

from .renderableentity import PageEntity, PostEntity
from .buildhandler import BuildHandler
from .rss import Rss
from .constants import CONFIG_XML_TEMPLATE
from .db import initialize_database, add_post


def parse_config(xml):
    config = {}
    
    doc = minidom.parseString(xml)
    config_nodes = doc.getElementsByTagName("config")
    config_node = config_nodes[-1]

    var_nodes = config_node.getElementsByTagName("var")
    for var in var_nodes:
        if var.hasAttribute("name") and var.hasAttribute("value"):
            n = var.getAttribute("name")
            v = var.getAttribute("value")
            config[n] = v
    return config
            
def build_project():

    logging.info("Removing build...")
    shutil.rmtree("build", ignore_errors=True)

    logging.info("Creating build folder...")
    os.mkdir("build")

    logging.info("Loading config...")
    with open("config.xml", "r") as filein:
        config = parse_config(filein.read())
    
    logging.info("Creating database...")
    con = sqlite3.connect("build/pslssg.db")
    cur = con.cursor()
    initialize_database(cur)

    #static
    logging.info("Copying static assets...")
    shutil.copytree("static", "build/static")

    renderable_entities = []
    
    #pages meta
    for root, dirs, files in os.walk("pages"):
        #create pages subdirectory tree
        for directory in dirs:            
            path_without_pages = os.path.relpath(os.path.join(root, directory), "pages")
            os.makedirs(os.path.join("build", path_without_pages))
        #create pages files
        for file in files:
            path_without_pages = os.path.relpath(root, "pages")
            if file.endswith(".xml"):
                logging.info("Parsing meta for {}/{}...".format(root, file))
                src = os.path.join(root, file)
                dst = os.path.join("build", path_without_pages, file.replace(".xml", ".xhtml"))
                renderable_entity = PageEntity(src, dst, cur, config)
                renderable_entity.parse_meta()
                renderable_entities.append(renderable_entity)
            else:
                logging.info("Skipping non .xml file {} ...".format(os.path.join(path_without_pages, file)))

    #posts meta
    for root, dirs, files in os.walk("posts"):
        #create posts subdirectory tree
        for directory in dirs:            
            os.makedirs(os.path.join("build", root, directory))
        for file in files:
            if file.endswith(".xml"):
                logging.info("Parsing meta for {}/{}...".format(root, file))
                src = os.path.join(root, file)
                dst = os.path.join("build", root, file.replace(".xml", ".xhtml"))
                renderable_entity = PostEntity(src, dst, cur, config)
                renderable_entity.parse_meta()
                renderable_entities.append(renderable_entity)
                add_post(con, cur, renderable_entity)
            else:
                logging.info("Skipping non .xml file {} ...".format(os.path.join(root, file)))

    #render posts and pages
    for renderable_entity in renderable_entities:
        if renderable_entity.is_draft == False:
            logging.info("Rendering {}".format(renderable_entity.dst))
            renderable_entity.render()

    #rss feed generation
    rss = Rss(cur, config)
    rss.add_items()
    rss.render()

def initialize_project():
    for folder in ["posts", "templates", "pages", "static"]:
        if os.path.isdir(folder):
            logging.warning("Folder {} already exists, skipping.".format(folder))
        else:
            os.mkdir(folder)

    with open("config.xml.template", "w") as fileout:
        fileout.write(CONFIG_XML_TEMPLATE)

def destroy_project():
    try:
        os.rmdir("build")
    except (FileNotFoundError, OSError) as e:
        pass

def serve_project():
    socketserver.TCPServer.allow_reuse_address = True
    with socketserver.TCPServer(("", 8080), BuildHandler) as httpd:
        print("serving at port 8080")
        try:
            httpd.serve_forever()
        except KeyboardInterrupt:
            pass

def main():
    parser = argparse.ArgumentParser(
        prog='pslssg',
        description='Generate websites from xhtml using the python standard library.',
        epilog='')

    parser.add_argument('subcommand')

    args = parser.parse_args()

    logging.basicConfig(level=logging.INFO)

    if args.subcommand == "init":
        initialize_project()
    elif args.subcommand == "build":
        build_project()
    elif args.subcommand == "destroy":
        destroy_project()
    elif args.subcommand == "serve":
        serve_project()
    else:
        logging.error("Invalid subcommand: " + args.subcommand)

if __name__ == "__main__":
    main()

