import sqlite3

def initialize_database(cursor):
    cursor.execute("""
    CREATE TABLE posts(
    id INTEGER PRIMARY KEY,
    src TEXT,
    dst TEXT,
    date TEXT,
    slug TEXT,
    title TEXT,
    is_draft INTEGER,
    template_name TEXT,
    summary TEXT
    )
    """)
    cursor.execute("""
    CREATE TABLE tags(
    tag TEXT,
    post_id INTEGER,
    FOREIGN KEY(post_id) REFERENCES posts(id)    
    )
    """)
    cursor.execute("""
    CREATE TABLE categories(
    category TEXT,
    post_id INTEGER,
    FOREIGN KEY(post_id) REFERENCES posts(id)
    )
    """)

def add_post(connection, cursor, post_entity):
    cursor.execute("""
    INSERT INTO posts (src, dst, date, slug, title, is_draft, template_name, summary) VALUES
    (?, ?, ?, ?, ?, ?, ?, ?)""", (
        post_entity.src,
        post_entity.dst,
        post_entity.date,
        post_entity.slug,
        post_entity.title,
        post_entity.is_draft,
        post_entity.template_name,
        post_entity.summary
    ))
    connection.commit()
    post_id = cursor.lastrowid

    for t in post_entity.tags:
        cursor.execute("""
        INSERT INTO tags (tag, post_id) VALUES
        ('{}', '{}')""".format(t, post_id))
    connection.commit()

    for c in post_entity.categories:
        cursor.execute("""
        INSERT INTO categories (category, post_id) VALUES
        ('{}', '{}')""".format(c, post_id))
    connection.commit()
