import unittest
from unittest.mock import MagicMock, patch

import xml.dom.minidom as minidom

from pslssg import renderableentity

def parse_meta_stub(s):
    pass

class TestXMLUtils(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_get_node_text(self):
        xml_doc = minidom.parseString('<sometag someattribute="somevalue">hello world</sometag>')
        xml_node = xml_doc.getElementsByTagName("sometag")[0]
        result = renderableentity.XMLUtils.get_node_text(xml_node)
        self.assertEqual("hello world", result)

    def test_clean_empty_text_nodes(self):
        xml_doc = minidom.parseString("""<sometag someattribute="somevalue">hello world

           
        </sometag>""")
        xml_node = xml_doc.getElementsByTagName("sometag")[0]
        renderableentity.XMLUtils.clean_empty_text_nodes(xml_node)
        result = renderableentity.XMLUtils.get_node_text(xml_node)
        self.assertEqual("hello world", result)

    def test_replace_node_with_node_children_from_other_file(self):
        source_doc = minidom.parseString('<sometag><replacetag>child text</replacetag></sometag>')
        other_doc = minidom.parseString('<othertag><otherchild>hello world</otherchild></othertag>')
        replace_node = source_doc.getElementsByTagName("replacetag")[0]
        other_node = other_doc.getElementsByTagName("othertag")[0]

        renderableentity.XMLUtils.replace_node_with_node_children_from_other_file(source_doc, replace_node, other_node)

        replace_tags = source_doc.getElementsByTagName("replacetag")
        other_tags = source_doc.getElementsByTagName("otherchild")
        self.assertEqual(0, len(replace_tags))
        self.assertEqual(1, len(other_tags))

        other_tag = other_tags[0]
        result = renderableentity.XMLUtils.get_node_text(other_tag)
        self.assertEqual("hello world", result)

    def test_replace_node_with_text_node(self):
        source_doc = minidom.parseString('<sometag><replacetag>child text</replacetag></sometag>')
        replace_node = source_doc.getElementsByTagName("replacetag")[0]

        renderableentity.XMLUtils.replace_node_with_text_node(source_doc, replace_node, "hello world")
        replace_tags = source_doc.getElementsByTagName("replacetag")
        self.assertEqual(0, len(replace_tags))
        some_tag = source_doc.getElementsByTagName("sometag")[0]
        result = renderableentity.XMLUtils.get_node_text(some_tag)
        self.assertEqual("hello world", result)

        
class TestRenderableEntity(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_postentity_parse_filename_meta(self):
        """
        Given a PostEntity with a src formatted /posts/category1/category2/YYYY_MM_DD_slug.xml,
        When parse_filename_meta is called,
        Then the date, year, month, day, slug, and categories fields are all set,
        Then variables are set for each field.
        """
        #with patch.object(renderableentity.PostEntity, "parse_meta", parse_meta_stub):
        sut = renderableentity.PostEntity("/posts/blog/tech/1999_12_31_happy_new_year.xml",
                                          "/dst",
                                          None,
                                          {},
        )
        
        sut.parse_filename_meta()
        self.assertEqual("1999_12_31", sut.date)

    def test_get_meta_templates(self):
        xml_node = minidom.parseString('<xml><template name="foo" /></xml>')
        sut = renderableentity.PostEntity("/posts/blog/tech/1999_12_31_happy_new_year.xml",
                                          "/dst",
                                          None,
                                          {},
        )
        sut.get_meta_template(xml_node)
        self.assertEqual("foo", sut.template_name)
