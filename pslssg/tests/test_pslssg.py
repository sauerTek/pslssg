import unittest

from pslssg import pslssg

class TestPslssg(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_parse_config(self):
        """
        Given a valid xml file,
        When parse_config is called,
        Then it should return a dict of name/value pairs from var tags.
        """
        xml = "<config><var name='foo' value='bar'/>><variable name='baz' value='boz'/></config>"
        result = pslssg.parse_config(xml)
        self.assertEqual("bar", result["foo"])
        self.assertNotIn("baz", result)
