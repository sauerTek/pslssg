import xml.dom.minidom as minidom
from datetime import datetime
from email import utils


class Rss(object):
    def __init__(self, db_cursor, config):
        self.db_cursor = db_cursor
        self.config = config

        now = datetime.now()
        datestring = utils.format_datetime(now)

        xmlstring = '<rss version="2.0">'
        xmlstring+= '<channel>'
        xmlstring+= '<title>{title}</title>'
        xmlstring+= '<link>{link}</link>'
        xmlstring+= '<description>{description}</description>'
        xmlstring+= '<language>en-us</language>'
        xmlstring+= '<pubDate>{date}</pubDate>'
        xmlstring+= '<lastBuildDate>{date}</lastBuildDate>'
        xmlstring+= '<docs>https://www.rssboard.org/rss-specification</docs>'
        xmlstring+= '<generator>pslssg</generator>'
        xmlstring+= '<managingEditor>{editor}</managingEditor>'
        xmlstring+= '<webMaster>{webmaster}</webMaster>'
        xmlstring+= '</channel>'
        xmlstring+= '</rss>'
        
        self.document = minidom.parseString(xmlstring.format(
            title=self.config["rss-title"],
            link=self.config["rss-link"],
            description=self.config["rss-description"],
            editor=self.config["rss-managingeditor"],
            webmaster=self.config["rss-webmaster"],
            date=datestring,
        ))

    def create_sub_node(self, element, text, parent):
        sub_node = self.document.createElement(element)
        sub_node_text = self.document.createTextNode(text)
        sub_node.appendChild(sub_node_text)
        parent.appendChild(sub_node)
        
    def add_items(self):
        channel_nodes = self.document.getElementsByTagName("channel")
        channel_node = channel_nodes[-1]
        
        res = self.db_cursor.execute("SELECT dst, date, title, summary FROM posts ORDER BY date DESC")
        items = res.fetchall()
        for item in items:
            pubdate = datetime.strptime(item[1], "%Y_%m_%d")
            pubdate_formatted = utils.format_datetime(pubdate)
            link = item[0].replace("build/", self.config["base-url"])
            
            new_item_node = self.document.createElement("item")
            self.create_sub_node("title", item[2], new_item_node)
            self.create_sub_node("link", link, new_item_node)
            self.create_sub_node("description", item[3], new_item_node)
            self.create_sub_node("pubDate", pubdate_formatted, new_item_node)
            self.create_sub_node("guid", link, new_item_node)
            #item
            channel_node.appendChild(new_item_node)
            

    def render(self):
        self.document.writexml(open("build/feed.xml", "w"), indent="  ", addindent="  ", newl="\n")
            
            
