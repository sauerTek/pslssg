"""Functionality for "Renderable Entities", aka, pages and posts in pslssg.

This is mostly XML operations for generating pages and posts.  There is a RenderableEntity
base class, two derived classes for pages and posts, and an XMLUtils class for common
XML manipulations.
"""

import sys
import xml.dom.minidom as minidom
import xml
import logging
import os
import importlib.metadata
import datetime

class XMLUtils(object):
    """A collection of common XML operations that the RenderableEntities use."""
    
    def get_node_text(xml_node):
        """
        Gets the first text node from an xml node.
        Params:
          - xml_node (xml.dom.Node) : The node to search.
        Returns:
          - (sting) : The string from the text node, with whitespace trimmed.
        Throws:
          - (ValueError) : If the node has no text node children.
        """
        for child in xml_node.childNodes:
            if child.nodeType == child.TEXT_NODE:
                return child.data.strip()
        else:
            raise ValueError("Node {} does not have text.".format(xml_node))

    def clean_empty_text_nodes(node):
        """
        Removes text node children that do not contain any text except whitespace. It
          also trims any text nodes that have trailing or beginning whitespace.
        Params:
          - node (xml.dom.Node) : The node to search children.
        Returns:
          - Nothing. This method operates on node directly.
        """
        queue = [node]
        while queue:
            current_node = queue.pop(0)
            to_remove = []
            for child in current_node.childNodes:
                if child.nodeType == child.TEXT_NODE:
                    if not child.data.strip():
                        to_remove.append(child)
                    else:
                        child.data = child.data.strip()
            for child in to_remove:
                current_node.removeChild(child)
            for child in current_node.childNodes:
                queue.append(child)

    def replace_node_with_node_children_from_other_file(source_doc, replace_node, other_node):
        """
        Imports a node from another document, and replaces a node in the source document with
          its children.
        Params:
          - source_doc (xml.dom.Document) - The source document with the node to replace.
          - replace_node (xml.dom.Node) - The node to replace entirely in the source_doc.
          - other_node (xml.dom.Node) - A node in another doc, whose children will replace
            replace_node in source_doc.
        Returns:
          - Nothing.  This method operates on source_doc directly.
        """
        parent = replace_node.parentNode
        for child in other_node.childNodes:
            newchild = source_doc.importNode(child, True) #undocumented method
            parent.insertBefore(newchild, replace_node)
        parent.removeChild(replace_node)

    def replace_node_with_text_node(source_doc, replace_node, text):
        """
        Replaces a node with a text node from a string.
        Params:
          - source_doc (xml.dom.Document) - The source document with the node to replace.
          - replace_node (xml.dom.Node) - The node to replace entirely in the source_doc.
          - text (string) - The string to create the text node from.
        Returns:
          - Nothing. This method opertates on source_doc directly.
        """
        parent = replace_node.parentNode
        text_node = source_doc.createTextNode(text)
        parent.insertBefore(text_node, replace_node)
        parent.removeChild(replace_node)

    def replace_node_with_node(source_doc, replace_node, new_node):
        """
        Replaces a node with another node
        Params:
          - source_doc (xml.dom.Document) - The source document with the node to replace.
          - replace_node (xml.dom.Node) - The node to replace entirely in the source_doc.
          - new_node (xml.dom.Node) - A node to replace the replace_node.
        Returns:
          - Nothing. This method opertates on source_doc directly.
        """
        parent = replace_node.parentNode
        parent.insertBefore(new_node, replace_node)
        parent.removeChild(replace_node)

    def load_xml(src):
        """
        Loads xml using minidom.parse. This is primary used so that it is easy to stub out in
          tests.
        Params:
          - src (string) : The filename of the XML file to parse.
        Returns:
          - (xml.dom.Document) : The XML document of the parsed file.
        """
        return minidom.parse(src)

class RenderableEntity(object):
    def __init__(self, src, dst, db_cursor, config):
        self.src = src
        self.dst = dst

        self.vars = dict(config) #default to copy of global vars
        
        self.date = ""
        self.year = ""
        self.month = ""
        self.day = ""
        self.slug = ""
        self.categories = ""
        self.title = ""
        self.summary = ""
        self.parse_filename_meta()
        
        self.template_name = ""
        self.is_draft = False
        self.tags = []
        self.entity_type = "entity"
        self.db_cursor = db_cursor        

    def __str__(self):
        return """RenderableEntity:
        src: {}
        dst: {}
        vars: {}
        template_name: {}
        slug: {}
        date: {}
        categories: {}
        isDraft: {}
        tags: {}
        title: {}
        summary: {}
        ---""".format(
            self.src, self.dst, self.vars, self.template_name, self.slug, self.date, self.categories, self.is_draft, self.tags, self.title, self.summary)

    def parse_filename_meta(self):
        raise NotImplementedError
        
    def get_renderable_toplevel_element(self, tag):
        """
        gets the highest xml tag in the content, which is <page> or <post>
        """
        try:
            doc = XMLUtils.load_xml(self.src)
        except xml.parsers.expat.ExpatError as e:
            logging.error("Error parsing content for {}, xml parse error: {}".format(self.src, e))
            sys.exit(1)
        
        try:
            page_elements = doc.getElementsByTagName(tag)
            page_element = page_elements[-1]
        except IndexError:
            logging.error("Error processing content for {}, no top level tag {}.".format(self.src, tag))
            logging.error("Aborting build.")
            sys.exit(1)
        return page_element

    def get_meta_template(self, xml_node):
        templates = xml_node.getElementsByTagName("template")
        try:
            template = templates[-1]
            if template.hasAttribute("name"):
                self.template_name = template.getAttribute("name")
            else:
                logging.warning("Template in meta is missing 'name' attribute.")
        except IndexError:
            logging.warning("Content at {} has no template defined.".format(self.src))
            sys.exit(1)
            
    def get_meta_vars(self, xml_node):
        vars = xml_node.getElementsByTagName("var")
        for var in vars:
            if var.hasAttribute("name") and var.hasAttribute("value"):
                n = var.getAttribute("name")
                v = var.getAttribute("value")
                self.vars[n] = v

    def get_meta_is_draft(self, xml_node):
        draft = xml_node.getElementsByTagName("draft")
        if len(draft) > 0:
            self.is_draft = True

    def get_meta_tags(self, xml_node):
        tags = xml_node.getElementsByTagName("tag")
        for tag in tags:
            if tag.hasAttribute("name"):
                self.tags.append(tag.getAttribute("name"))

    def get_meta_title(self, xml_node):
        titles = xml_node.getElementsByTagName("title")
        if len(titles) < 1:
            if self.entity_type == "post":
                logging.warning("Post at {} has no title; slug will be used.".format(self.src))
            self.title = self.slug
        else:
            title = titles[-1]
            self.title = XMLUtils.get_node_text(title)

    def get_meta_summary(self, xml_node):
        summaries = xml_node.getElementsByTagName("summary")
        if len(summaries) > 0:
            summary = summaries[-1]
            self.summary = XMLUtils.get_node_text(summary)
                        
    def parse_meta(self):
        toplevel_element = self.get_renderable_toplevel_element()
        template_name = "default.xhtml"
    
        meta_elements = toplevel_element.getElementsByTagName("meta")
        try:
            meta = meta_elements[-1]
            self.get_meta_template(meta)
            self.get_meta_vars(meta)
            self.get_meta_is_draft(meta)
            self.get_meta_tags(meta)
            self.get_meta_title(meta)
            self.get_meta_summary(meta)

            #populate vars with meta
            self.vars["title"] = self.title
            self.vars["is_draft"] = str(self.is_draft)
            self.vars["tags"] = self.tags
            self.vars["_pslssg_version"] = importlib.metadata.version('pslssg')
            self.vars["_pslssg_build_date"] = datetime.datetime.now().strftime("%b %d, %Y")
        except IndexError: #no meta tag
            logging.error("Content at {} contains no meta tag.".format(self.src))
            sys.exit(1)
            
    def render(self):
        toplevel_element = self.get_renderable_toplevel_element()
        #get content tag element from post
        content_elements = toplevel_element.getElementsByTagName("content")
        try:
            content_element = content_elements[-1]
        except IndexError: #no content tag
            logging.error("Error processing page {}, no content specified.".format(self.src))
            logging.error("Aborting build.")
            sys.exit(1)

        content = self.parse_content(self.template_name, content_element)
        XMLUtils.clean_empty_text_nodes(content)
        content.writexml(open(self.dst, 'w'), indent="  ", addindent="  ", newl="\n")
        content.unlink()

    def process_template_includes(self, template_doc):
        template_includes = template_doc.getElementsByTagName("pslssg-include")
        for template_include in template_includes:
            parent = template_include.parentNode
            try:
                include_template_name = template_include.attributes["template"].value
                include_doc = XMLUtils.load_xml(os.path.join("templates", include_template_name))
                #add all children of content before <pslssg-include >, then delete <pslssg-include >
                XMLUtils.replace_node_with_node_children_from_other_file(template_doc, template_include, include_doc)             
            except KeyError:
                logging.warning("Include in {} does not have 'template' attribute.".format(template_file))

    def process_template_content(self, template_doc, content_element):
        template_contents = template_doc.getElementsByTagName("pslssg-content")
        try:
            template_content = template_contents[-1]
            parent = template_content.parentNode
            if parent == None:
                logging.error("Error processing page {}, template {} <pslssg-content /> tag is at root of document.".format(self.src, template_file))
                logging.error("Aborting build.")
                sys.exit(1)
            #add all children of content before <pslssg-content />, then delete <pslssg-content />
            XMLUtils.replace_node_with_node_children_from_other_file(template_doc, template_content, content_element)             
        except IndexError:
            logging.error("Error processing page {}, template {} does not have <pslssg-content /> tag.".format(self.src, template_file))
            logging.error("Aborting build.")
            sys.exit(1)

    def process_template_vars(self, template_doc):
        template_vars = template_doc.getElementsByTagName("pslssg-var")
        for var in template_vars:
            parent = var.parentNode
            if parent == None:
                logging.error("Error processing page {}, template {} <pslssg-var /> tag is at root of document.".format(self.src, template_file))
                logging.error("Aborting build.")
                sys.exit(1)
            #add text node of var to <pslssg-var/>, then delete <pslssg-var />
            name = var.attributes["name"].value
            for n, v in self.vars.items():
                if n == name:
                    XMLUtils.replace_node_with_text_node(template_doc, var, v)

    def process_template_iterations(self, template_doc):
        template_iters = template_doc.getElementsByTagName("pslssg-iter")
        for iter in template_iters:
            parent = iter.parentNode
            if parent == None:
                logging.error("Error processing renderable {}, template {} <pslssg-iter> tag is at root of document.".format(self.src, template_file))
                logging.error("Aborting build.")
                sys.exit(1)
            # perform query
            if "query" in iter.attributes:
                query = iter.attributes["query"].value
                res = self.db_cursor.execute(query)
                items = res.fetchall()
            elif "var" in iter.attributes:
                var = iter.attributes["var"].value
                try:
                    items = [ [x] for x in self.vars[var]]
                except KeyError:
                    logging.warning("Template references iter var '{}' which does not exist in self.vars".format(var))
                    items = []
                if type(items) is not list:
                    logging.warning("Template references iter var '{}' which is not a list.".format(var))
                    items = []

            #for each row of the query:
            for i in items:
                for child in iter.childNodes:
                    #for each child of pslssg-iter (aka iter), clone it and place above pslssg-iter
                    newchild = child.cloneNode(True)
                    parent.insertBefore(newchild, iter)
                    
                    #search the new child for pslssg-iter-items to process
                    #ignore text nodes in this process
                    #or, if the child itself is pslssg-iter-item, process it
                    if newchild.nodeType == newchild.TEXT_NODE:
                        iter_items = []
                    elif newchild.nodeName == "pslssg-iter-item":
                        iter_items = [newchild]
                    else:
                        iter_items = newchild.getElementsByTagName("pslssg-iter-item")
                        
                    #for each pslssg-iter-item you find in the child tags, process
                    for iter_item in iter_items:
                        set_attributes = {}
                        tag = ""
                        text = ""
                        
                        for attribute_name, attribute_value in iter_item.attributes.items():
                            if attribute_name == "_tag":
                                tag = attribute_value
                            elif attribute_name == "_text":
                                text = attribute_value
                            else:
                                set_attributes[attribute_name] = attribute_value
                        new_tag = template_doc.createElement(tag)
                        for new_attribute_name, new_attribute_value in set_attributes.items():
                            new_tag.setAttribute(new_attribute_name, new_attribute_value.format(*i))
                        #text will be format string. i (query row) expanded to args for format
                        new_tag_text = template_doc.createTextNode(text.format(*i))

                        new_tag.appendChild(new_tag_text)
                        XMLUtils.replace_node_with_node(template_doc, iter_item, new_tag)

            #Delete pslssg-iter and all of its children, their clones inserted above it
            parent.removeChild(iter)
        
    def parse_content(self, template_name, content_element):
        # parse template xml, store as var template_doc
        template_file = os.path.join("templates", template_name)
        if os.path.isfile(template_file):
            try:
                template_doc = XMLUtils.load_xml(template_file)
            except xml.parsers.expat.ExpatError as e:
                logging.error("Error prossessing template '{}', failed with error: {}".format(template_file, str(e)))
                logging.error("Aborting build.")
                sys.exit(1)
        else:
            logging.error("Error processing page {}, could not find template {}.".format(self.src, template_file))
            logging.error("Aborting build.")
            sys.exit(1)

        self.process_template_includes(template_doc)
        self.process_template_content(template_doc, content_element)
        self.process_template_vars(template_doc)
        self.process_template_iterations(template_doc)
                
        # perform inheritance by looking for pslssg-extends
        super_templates = template_doc.getElementsByTagName("pslssg-extends")
        try:
            super_template = super_templates[-1] #can only have one extension, use last defined
            super_template_name = super_template.attributes["template"].value
            parent = super_template.parentNode
            if parent == None:
                logging.error("Error processing page {}, template {} <pslssg-extends /> tag is at root of document.".format(self.src, template_file))
                logging.error("Aborting build.")
                sys.exit(1)
            else:
                parent.removeChild(super_template)
                #parse the super template, using the currently rendered template_doc as its content
                template_doc = self.parse_content(super_template_name, template_doc) 
        except IndexError:
            pass #if no super template, thats ok
        
        return template_doc

class PageEntity(RenderableEntity):
    def __init__(self, src, dst, db_cursor, config):
        super().__init__(src, dst, db_cursor, config)
        self.entity_type = "page"

    def parse_filename_meta(self):
        filename = os.path.split(self.src)[1]
        filename_no_ext = filename.split(".")[0]
        self.slug = filename_no_ext
        self.vars["slug"] = self.slug

    def get_renderable_toplevel_element(self):
        return super().get_renderable_toplevel_element("page")
        
class PostEntity(RenderableEntity):
    def __init__(self, src, dst, db_cursor, config):
        super().__init__(src, dst, db_cursor, config)
        self.entity_type = "post"

    def parse_filename_meta(self):
        filename = os.path.split(self.src)[1]
        filename_no_ext = filename.split(".")[0]
        parts = filename_no_ext.split("_")
        if len(parts) < 4:
            logging.error("Bad filename for {}, must be YYYY_MM_DD_slug.xml".format(self.src))
            sys.exit(1)
        self.date = "_".join(parts[0:3])
        self.year = parts[0]
        self.month = parts[1]
        self.day = parts[2]
        self.slug = "_".join(parts[3:])

        self.vars["date"] = self.date
        self.vars["year"] = self.year
        self.vars["month"] = self.month
        self.vars["day"] = self.day
        self.vars["slug"] = self.slug
        
        path = os.path.split(self.src)[0]
        parts = path.split(os.path.sep)
        self.categories = parts[1:] #first part would be 'posts'

    def get_renderable_toplevel_element(self):
        return super().get_renderable_toplevel_element("post")
