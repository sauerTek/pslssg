CONFIG_XML_TEMPLATE = """<?xml version="1.0"?>
<config>
  <!-- rss vars -->
  <var name="rss-title" value="Title of RSS Feed" />
  <var name="rss-link" value="Homepage of feed content" />
  <var name="rss-description" value="Description of feed" />
  <var name="rss-managingeditor" value="Editor email address" />
  <var name="rss-webmaster" value="Webmaster email address" />

  <!-- other vars -->
  <var name="global-var-test" value="global-value" />
</config>"""
