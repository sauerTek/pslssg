# pslssg

A static site generator using the Python standard library, using XML as the template and content format.

# Developer Instructions

First, create a virtual environment called "env" and activate it:
    python3.11 -m venv env
    source env/bin/activate.fish

While pslssg does not require any external dependencies to use, it does require a few tools to develop it.
    
You'll need Python's 'build' package from pip:
    pip install build

To build pslssg:
    python -m build 

This builds a .whl and a .tar file in the dist folder.

To manually test, create a folder elsewhere on your computer, create another virtual environment, activate it, and install pslssg with:
    pip install -e <relative path to pslssg>

This installs pslssg in "editable develop mode" and changes you make to the package will be instantly reflected without having to rebuild.

To run the unit tests, run:
    python3 -m unittest discover -s pslssg/tests

# User Instructions

## Introduction

pslssg is a static site generator that can create blog style websites that do not require a running database.  To create a site, a user will create pieces of content in a specific folder structure, and the program will use the data it finds to generate HTML pages.  pslssg uses XML as both the input data (XML files that describe content and its metadata) as well as its output data (XHTML files).

pslssg is heavily inspired by Jeckyll, which is written in Ruby and uses Markdown as the input format and HTML as the output format.

## Creating a Project

### Project Folder Structure

Each project needs the following folders:

- `posts` - This folder contains content that is dynamic.  Subfolders to this folder specify a piece of content's "category", allowing sites to have differt types of content, like blog and news content.
- `pages` - This folder contains pages of a website that are outside of the content pages, like a homepage, about page, or contact page.  These pages are rendered the same way posts are, so they can make use of templates, includes, and variables.
- `templates` - This folder contains templates, which are wrappers around content.  Page and Post content are rendered and inserted into templates.  Templates can also use inheritance to place the resulting template contents into a template a level above in the chain.
- `static` - This folder will be copied into the new site as-is, in a folder called `static`.  So, this is where you can put images, stylesheets, scripts, and other truly static content.

You can create this folder structure by running: `pslssg init`.

### Project Config

The `pslssg init` command will also create a file, `config.xml.template`.  To configure your project, copy this file and rename it `config.xml`.  These settings are mostly used by the rendering engine for metadata related to the RSS feed, but you are able to put any global variables into this file as well.

## Creating Content

To create a post, create a subfolder in the `posts` directory, and add a file with the format `YYYY_MM_DD_slug.xml`, where "slug" is a URL compliant string.  This naming convention allows pslssg to set the publish date metadata and slug.  Inside the post file, there should be this XML format:

```
<?xml version="1.0"?>
<post>
  <meta>
    ... see meta section details
  </meta>
  <content>
    ... see content section details
  </content>
</post>
```

Pages are the same as posts, except for the toplevel element is "page" instead of "post".  Pages are also created without categories; their structure in the `pages` directory is directly copied over to the rendered site.

### Meta Section

The meta section contains information about content that is needed for its rendering.  It can contain the following XML elements:

#### Templates

Each piece of content must specify a template, which is the wrapping XHTML to place the rendered content into.  Specify a template using a tag: `<template name="template-name.xhtml" />`, where the value of the name attribute is a filename in the `templates` directory.

For more about templates, see the section below.

#### Vars

Variables can be added in the meta section that will be replaced inside of the template and content when rendered.  A tag like: `<var name="variable-name" value="variable-value" />` will create a variable that can be used in content and teplates by using a tag like `<pslssg-var name="variable-name" />`.

Variables added in the meta section only apply to the content it appears in.  For global variables, add `<var>` tags to `config.xml`.

Vars are optional.  When content is rendered, pslssg automatically adds the following variables:

- title: the title of the content
- is_draft: either "True" or "False" if the content is a draft.
- tags: a list of tags.
- _pslssg_Version: the version of pslssg used to render this content
- _pslssg_build_date: the date the content was rendered.

#### Tags

Each piece of content can have any number of tags associated with it.  This is an additional categorizing functionality in addition to the subfolder category name.  To specify tags, use one or more `<tag>` elements:

```
<tag name="blog">
<tag name="tech">
```

Tags are optional.

#### Drafts

If a piece of content is not ready for publication, but you want to build the site, you can specify a piece of content is a drafy by adding a `<draft />` tag inside the meta section.  This will cause the renderer to skip over the rendering of content.

#### Title

A piece of content can have a title associated with it.  Use a tag like: `<title>My Awesome Blog Post</title>` to set the title.  Titles are optional, when they are not specified, the title will be the slug specified in the filename.

#### Summary

A piece of content can have a summary, which can be used to preview content.  Specify a summary by using a tag like: `<summary>This is my content summary!</summary>`.

Summaries are optional, and default to a blank string.

### Content Section

#### Using Vars

Using variables can be done by using a tag like `<pslssg-var name="variable-name" />`.  Variables can be specified in content metadata or the global config file (config.xml).

#### Using Iter Tags

## Templates

### Template Inheritance

### Template Includes

### Template Vars and Iter Tags

## Building and Serving the Site

To build the site, run the command `pslssg build` in the root of your site.  This will create a folder called "build" that will contain the rendered site. `pslssg destroy` will delete the build directory, but won't delete your posts, pages, templates, and static assets.

To view the site, run `pslssg serve`.  This will start a server at `localhost:8080` so that you can view your site.

### Special pslssg Variables

### RSS Generation

Building the site also builds a standards compliant RSS feed.  It will be created in a file called `feed.xml` in the root of your site.

## Examples


